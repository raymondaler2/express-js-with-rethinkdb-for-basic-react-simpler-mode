//@ts-ignore
import rethinkdbdash from "rethinkdbdash";

export const db = rethinkdbdash({
  db: "form",
  host: "localhost",
  port: "28015",
});
