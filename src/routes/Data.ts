import express from "express";
export const DataRoutes = express.Router();

import { DataControls } from "../controls/Data";

DataRoutes.get("/Data", DataControls.getAllData);
DataRoutes.post("/Data", DataControls.insertInput);
DataRoutes.delete("/Data/:id", DataControls.deleteData);
DataRoutes.put("/Data", DataControls.updateData);
