import { Response, Request } from "express";
import { DataModel } from "../model/Data";

export const DataControls = {
  getAllData: async (req: Request, res: Response) => {
    const result = await DataModel.getAll();
    return res.send(result);
  },

  insertInput: async (req: Request, res: Response) => {
    const data = req.body;
    const result = await DataModel.insert(data);
    if (result.inserted) {
      return res.status(200).send({
        message: "Input Inserted",
        code: 200,
      });
    } else {
      return res.status(500).send({
        message: "Input Insert Failed!",
        code: 500,
      });
    }
  },

  deleteData: async (req: Request, res: Response) => {
    const { id } = req.params;
    const result = await DataModel.delete(id);
    if (result.deleted) {
      return res.status(200).send({
        message: "Data Deleted",
        code: 200,
      });
    } else {
      return res.status(500).send({
        message: "Delete Failed!",
        code: 500,
      });
    }
  },

  updateData: async (req: Request, res: Response) => {
    const data = req.body;
    const result = await DataModel.update(data);
    if (result.replaced) {
      return res.status(200).send({
        message: "Data Updated",
        code: 200,
      });
    } else {
      return res.status(500).send({
        message: "Update Failed!",
        code: 500,
      });
    }
  },
};
