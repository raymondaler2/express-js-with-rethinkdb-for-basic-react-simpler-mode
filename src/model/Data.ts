import { db } from "../../db";

export const DataModel = {
  getAll: async () => {
    const result = await db.table("result").run();
    return result;
  },
  insert: async (Load: any) => {
    const result = await db.table("result").insert(Load).run();
    return result;
  },
  delete: async (id: any) => {
    const result = await db.table("result").get(id).delete().run();
    return result;
  },
  update: async (Load: any) => {
    const { id } = Load;
    const result = await db.table("result").get(id).update(Load).run();
    return result;
  },
};
