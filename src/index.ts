import express from "express";
const app = express();
const PORT_NUMBER: number = 4000;

import { DataRoutes } from "./routes/Data";
app.use(express.json());
app.use(DataRoutes);

app.listen(PORT_NUMBER, () =>
  console.log(`Running in http://localhost:${PORT_NUMBER}`)
);
